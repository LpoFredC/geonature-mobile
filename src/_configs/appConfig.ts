class AppConfig {
  appId = 3;
  apiUrl = "https://demo.geonature.fr/geonature/api";
}

const appConfig = new AppConfig();
export { appConfig };
